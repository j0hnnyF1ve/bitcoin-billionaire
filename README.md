#### Firebase Public Link
https://bitcoin-billionaire.firebaseapp.com

# ------------------------------------------------
# Getting Started
# ------------------------------------------------
#### First, run npm install to get dependencies
```npm install```

#### To clean and create a new build for distribution:
```npm run build```

#### To run the watcher while doing development, run this command in a separate terminal:
#### The watcher allows for live updates of the local webserver
```npm run watch```

#### Then, to run a local webserver for development, run this command in a separate terminal:
```npx http-server ./dist -p 8080```


### ------------------------------------------------
### Firebase Deployment
### ------------------------------------------------
NOTE: Requires the [Firebase CLI](https://firebase.google.com/docs/cli)

#### To test firebase locally:
```firebase serve```

#### To deploy to firebase
```firebase deploy```

#### For issues with authentication/permissions, try:
```
firebase logout
firebase login --reauth
```
[Source](https://stackoverflow.com/questions/52891500/http-error-401-while-setting-up-firebase-cloud-functions-for-android-project)

# ------------------------------------------------
# A. Description
# ------------------------------------------------

Bitcoin Billionaire is a derivation of an idle sim game such as AdVenture Capitalist.

The Game Engine works as a state machine, and works like this: If we save the current total profits, and the last update times, we should be able to get the new profit total using an equation like the following:

```
R = Current rate, per unit of work (i.e. click on work button)
I = Intervals, number of intervals that have taken place since the last profit calculation

Total Profit = (C1R x C1I) + (C2R x C2I) + (C2R x C2I) +  . . . + + (CnR x CnI)
```

So we should be able to retrieve the current profits at any point in the future.


Data flows unidirectionally from the Game Engine to the UI.  The UI can issue commands that will then update the game engine.  It's probably similar to Redux, and similar modern patterns, in that data flow is unidirectional.  

# ------------------------------------------------
# B. Focus
# ------------------------------------------------

My focus was on building a Front End client for the Game Engine I built, but it can be adapted to back-end or a server by running the Game Engine on a server or in the cloud (i.e. Lambda or Google App Engine), and controlling the game using the command line.

The game UI should work in both desktop and mobile apps, the scrolling is intentional as 
I wanted there to be more user interaction during "live" game play.

User session is persisted in the localStorage of the browser via a state save, which makes it trivial to pick up where the user left off. Of course, in a production app, I would probably not have the localStorage operate as the "single source of truth", as it would be trivial to modify.

I didn't really see a need to have a back end for this app, unless in a future version we would want to persist user information.

# ------------------------------------------------
# C. Reasoning
# ------------------------------------------------

The most important part of the app for me was getting the game engine right.  So I invested most of my time in making sure that the state machine part of the game engine worked as intended, as I felt things could go wrong with the UI, but as long as the game engine works, then the UI can be updated to reflect the current state of the game engine.

The Game Engine (M), UI (V), and Handlers (C) are three pieces of a MVC type system.  By designing it this way, the three pieces are not tightly coupled with each other, and the UI/Handlers could be swapped out if we want to change the game environment (front end to back end for instance).

We can also port over the Game Engine to a diff environment / UI as well because of the very loose coupling.

Having three distinct components also allows for easier automated testing later.


I also focused on refining the front end a bit, but I found this harder in practice, as I think in order to have a finely tuned UI, the idea for the design has to be there in the beginning.


# ------------------------------------------------
# D. Tradeoffs
# ------------------------------------------------

By making the entire game engine a state machine, we can retrieve the state of the game at any time.

This is an improvement over the AdVenture Capitalist game, which can get out of state in certain situations, such as on the loading screen, where the game doesn't update until you click through. 

AdVenture Capitalist also seems to operate on two tiers, a real time tier where it's updating in real time, and last saved tier where the game resumes from a previous state, similar to my solution. So part of the problem is that when playing live, the live sim has to be "activated", whereas I believe that any given point of time, barring user interaction, we should be able to retrieve the current profits.


For tech implementation, I decided not to go with a framework such as React because I was finding that it would add extra layer of complexity, since React has it's own event loop on top of the event loop native to the Browser, and I knew that I wanted to run an update of the game at regular intervals.

If I had more time (like a week), I would move the UI to Canvas, as I find the browser will probably choke from too many DOM updates once the game scales up.

I would use BigInt or another big number implementation for a final release, and perform exhaustive testing for number ranges we would support in this game, as well as tweaking the game params to make sure the game would not exceed its limits as it approached them.


I didn't have time, but I wanted to write pretty thorough unit tests for the gameEngine, as well
as integration/scenario based testing, as it would make tweaking the game mechanics much easier.
This would be the very next thing I would add now that I'm at point where the game is at a good state.



## ------------------------------------------------
# Development Notes
## ------------------------------------------------
* Upgrading - Upgrading is slightly different than AdVenture Capital, in that it both reduces the interval between each unit of work, and raises the value of each unit of work.  
  * Reasoning: I did this to try to simplify the UI, and I found having to open dialogs for certain things, such as upgrades, a bit cumbersome.

* Development done mostly in Chrome, play tested on Chrome on Mac OSX and a Google Pixel 3a for mobile.

* Reset button is present mostly for testing on mobile, since dev tools are not as easily accessible there.

### ------------------------------------------------
### Src Files Description
### ------------------------------------------------

- css
  - custom.css - Stylesheet for the app
- img - image assets for the app
- js
  - components
    - gameEngine.mjs - The Game Engine module that runs the whole game
    - handlers.mjs - Event handlers for the UI
    - helpers.mjs - Helper functions shared throughout the app
    - ui.mjs - User Interface engine
  - main.js - Entry point to the app, contains the init, exit functions, animation loop.
  - plugins.js - not used, but this would be any add'l plugins necessary for the app would go

## ------------------------------------------------
## Proposed Improvements
## ------------------------------------------------

* Tests, more tests
* Balancing: Making the game fun, making the upgrade/hire manager/rate of increase configurable
* Use BigInt so we can support values larger than 2^52
* UI is inefficient, redraws most elements every cycle, change to only update UI when change is detected
   * Can probably save prev state and compare hashes with current state
   * Canvas based UI may be more efficient, HTML5 version may be suffering from too many redraws when game progresses and model updates are more frequent
* CI/CD to deploy to local and remote servers, and to incorporate a testing step into every release
* Integrate SASS into Gulp build
* Minify/Uglify to package up js into a compressed, space efficient version
* Backend version of game (text based, put the game engine behind an API and query it for the current state, and issue commands to it)
* Configuration File to set up different types of games (Currently tied to a single config hard coded into main.js)


## ---------------------------------------------------------------
## Misc & Resources
## ---------------------------------------------------------------

#### Built from HTML5 Boilerplate:
[HTML5 Boilerplate](https://html5boilerplate.com/)

#### License
The code is available under the [MIT license](LICENSE.txt).

[Google App Engine](https://cloud.google.com/appengine/docs/standard/python/getting-started/hosting-a-static-website)

[Firebase Project Console](https://console.firebase.google.com/project/bitcoin-billionaire/overview)

[Sources for Images (Pixabay, free for commercial use w/o attribution)](https://pixabay.com/images/search/bitcoin/)