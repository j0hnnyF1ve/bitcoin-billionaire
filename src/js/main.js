import { createCompany, GameEngine } from './components/gameEngine.mjs';
import { Helpers } from './components/helpers.mjs';
import { UI } from './components/ui.mjs';
import { Handlers } from './components/handlers.mjs';

const companyTemplate = `<section class="company" 
  data-index="{index}" 
  >
  <section class="active hide">
    <h1 class="name">{name}</h1>
    <div class="buttons">
      <div class="left">
        <button title="Upgrade" class="upgradeBusiness" ><img class="upgrade" src="img/upgrade.png" /> <img class="symbol" src="img/bitcoin-symbol.png" /><span class="upgradePrice">{upgradePrice}</span></button>
        <button title="Auto Miner" class="hireManager" ><img class="autominer_1" src="img/autominer_1.png" /> <img class="symbol" src="img/bitcoin-symbol.png" /><span class="managerCost">{managerCost}</span></button>
      </div>
      <div class="right">
        <button title="Work" class="work" ><img class="miner" src="img/miner.png" /><span class="rate">{rate}</span></button>
      </div>
    </div>
    <div>
      <progress class="" value="70" max="100"></progress>
    </div>
    <div>
      <label class="hide">Profit: <span class="profit">{profit}</span>, </label>
      <label>Mining Rate: <span class="rate">{rate}</span> per <span class="interval">{interval}</span> second(s)</label>
    </div>
  </section>
  <section class="inactive">
    <h1 class="name">{name}</h1>
    <div>
      <button title="Buy Business" class="buyBusiness">Buy for <img class="symbol" src="img/bitcoin-symbol.png" />{businessCost}</button>
    </div>
  </section>
</section>`;

const companiesList = [
  createCompany('Bitcoin Miner #1', true, 0, 10, 1, 10, 1000),
  createCompany('Bitcoin Miner #2', false, 10, 2000, 3, 30, 2000),
  createCompany('Bitcoin Miner #3', false, 100, 5000, 7, 50, 4000),
  createCompany('Bitcoin Miner #4', false, 500, 10000, 9, 50, 4000),
  createCompany('Super Bitcoin Miner #4', false, 100000, 20000, 100, 500, 1500)
];

const theGameEngine = GameEngine(companiesList);
const theHelpers = Helpers();
const theUI = UI(theGameEngine, theHelpers);
const theHandlers = Handlers(theGameEngine, theUI, theHelpers);

let stopOrdered = false;
let lastUpdated = Date.now();
const runGameLoopFunc = ((gameEngine, UI) => () => {
  // stop the update
  if(stopOrdered === true) { return; }

  // Aiming for 60hz cycle refresh of UI
  if( (Date.now() - lastUpdated) > 16.67 ) {
    lastUpdated = Date.now();
    gameEngine.checkManagers();
    UI.update();  
  }

  window.requestAnimationFrame(runGameLoopFunc);  
})(theGameEngine, theUI);
const stopUpdate = () => stopOrdered = true;
const startUpdate = () => { stopOrdered = false; runGameLoopFunc(); };

// Exposing modules on the global space for testing and tweaking currently
window.gameEngine = theGameEngine;
window.helpers = theHelpers;
window.UI = theUI;
window.Handlers = theHandlers;

window.stopUpdate = stopUpdate;
window.startUpdate = startUpdate;


// Initialize the layout by adding the companies
window.addEventListener('load', ((gameEngine, UI, Handlers, runGameLoop, Helpers) => (e) => {

  // Instantiate the initial UI
  let companiesHtml = gameEngine.getAllCompanies().map( (company, index) => {
    const { active, name, businessCost, managerCost, baseRate, baseUpgradePrice, baseInterval } = company;

    let el = document.createElement('div');
    
    let myTemplate = companyTemplate;
    myTemplate = myTemplate.replace(/{index}/g, index);
    myTemplate = myTemplate.replace(/{businessCost}/g, businessCost.toFixed(2));
    myTemplate = myTemplate.replace(/{managerCost}/g, managerCost.toFixed(2));
    myTemplate = myTemplate.replace(/{upgradePrice}/g, baseUpgradePrice.toFixed(2));
    myTemplate = myTemplate.replace(/{name}/g, name);
    myTemplate = myTemplate.replace(/{profit}/g, 0);
    myTemplate = myTemplate.replace(/{rate}/g, baseRate.toFixed(2));
    myTemplate = myTemplate.replace(/{interval}/g, baseInterval);
    el.innerHTML = myTemplate;

    if(active === true) {
      Helpers.showEl(el.querySelector('.company .active'));
      Helpers.hideEl(el.querySelector('.company .inactive'));
    }

    document.querySelector('.companies').appendChild(el.querySelector('.company'));
  });

  document.querySelectorAll('.upgradeBusiness').forEach( el => el.addEventListener('click', Handlers.upgradeBusiness) );
  document.querySelectorAll('.hireManager').forEach( el => el.addEventListener('click', Handlers.hireManager) );
  document.querySelectorAll('.work').forEach( el => el.addEventListener('click', Handlers.work) );
  document.querySelectorAll('.buyBusiness').forEach( el => el.addEventListener('click', Handlers.buyBusiness) );
  document.querySelectorAll('.reset').forEach( el => el.addEventListener('click', Handlers.reset) );

  gameEngine.loadFromStorage();
  runGameLoop();
  UI.update();
})(theGameEngine, theUI, theHandlers, runGameLoopFunc, theHelpers) );

window.addEventListener('unload', ( gameEngine => e => gameEngine.saveToStorage() )(theGameEngine) );
