/**************************************************************************
UI - The UI consumes data from the gameEngine and updates the User Interface
* Think of the "V" in MVC
**************************************************************************/

const UI = (gameEngine, Helpers) => {
  let prevCompanies = [];
  let prevTotal = -99;

  const update = () => {
    const companyEls = Array.from(document.querySelectorAll('.company'));
    
    const totalProfit = gameEngine.getTotalProfit();
    
    // Company Updates here
    gameEngine.getAllCompanies().forEach( (company, index) => {
      const companyEl = companyEls[index];

      // Previous company copy to do our comparison
      const prevCompany = Object.assign({}, prevCompanies[index]);

      if(company.changed === true) { gameEngine.setChanged(index, false); }

      companyEl.querySelector('progress').value = gameEngine.getProgress(index);

      // Update the company with its current upgrade price, rate, and interval
      if(company.currentUpgradePrice !== prevCompany.currentUpgradePrice) {
        companyEl.querySelector('.upgradePrice').innerHTML = company.currentUpgradePrice.toFixed(2);
      }
      if(company.currentRate !== prevCompany.currentRate) {
        companyEl.querySelectorAll('.rate').forEach( el => el.innerHTML = company.currentRate.toFixed(2) ); 
      }
      if(company.currentInterval !== prevCompany.currentInterval) {
        companyEl.querySelector('.interval').innerHTML = (company.currentInterval / 1000).toFixed(2);  
      }

      // Show glow animations for upgrades available
      companyEl.querySelector('.upgradeBusiness').className = (totalProfit >= company.currentUpgradePrice) ? 'upgradeBusiness available' : 'upgradeBusiness';
      companyEl.querySelector('.hireManager').className = (company.managerHireTime == null && totalProfit >= company.managerCost) ? 'hireManager available' : 'hireManager';
      companyEl.querySelector('.buyBusiness').className = (company.active === false && totalProfit >= company.businessCost) ? 'buyBusiness available' : 'buyBusiness';

      // If company has been bought, display the active panel
      if(company.active !== prevCompany.active) {
        if(company.active === true) {
          Helpers.hideEl( companyEl.querySelector('.inactive') );
          Helpers.showEl( companyEl.querySelector('.active') );
        }
        else {
          Helpers.showEl( companyEl.querySelector('.inactive') );
          Helpers.hideEl( companyEl.querySelector('.active') );
        }
      }

      // If a manager has been hired
      if(company.managerHireTime !== prevCompany.managerHireTime) {
        if(company.managerHireTime != null) {
          companyEl.querySelector('.hireManager').disabled = true;
          companyEl.querySelector('.hireManager').innerHTML = '<img class="autominer_1" src="img/autominer_1.png" /> On';
  
          Helpers.hideEl( companyEl.querySelector('.work') );
        }
        else {
          companyEl.querySelector('.hireManager').disabled = false;
          companyEl.querySelector('.hireManager').innerHTML = `<img class="autominer_1" src="img/autominer_1.png" /> <img class="symbol" src="img/bitcoin-symbol.png" /><span class="managerCost">${company.managerCost.toFixed(2)}</span>`;
  
          Helpers.showEl( companyEl.querySelector('.work') );
          companyEl.querySelector('.work').className = 'work';
          companyEl.querySelector('.work').disabled = false;
        }
      }

    });
  
    // Update the Total Price Display
    if(prevTotal !== gameEngine.getTotalProfit() ){
      const totalEl = document.querySelector('.total');
      totalEl.innerHTML = gameEngine.getTotalProfit().toFixed(2);
    }
    
    // cache previous copies for the next iteration
    prevCompanies = gameEngine.getAllCompanies();
    prevTotal = gameEngine.getTotalProfit();
  };
  return { update };
};

export { UI };