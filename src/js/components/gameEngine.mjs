/**************************************************************************
createCompany - Pass in the default parameters for a given company.
May change this in the future to expect a parameter as an object, as the method signature may
become hard to understand once we add more parameters

Fields:
* active              - indicates if the company should be active by default
* businessCost        - The amount to "buy" the business
* name                - Name of the company, can be displayed on the UI
* changed             - Boolean, if the company changed between intervals
* managerHireTime     - Timestamp of the manager hire, indicates that a manager has been hired
* managerCost         - The amount to acquire an auto manager for the business
* lastUpdated         - Timestamp of the last time work was recorded
* baseRate            - The starting unit of work
* baseUpgradePrice    - The starting base upgrade
* baseInterval        - The starting interval (in ms) before one unit of work is completed
* currentProfit (deprecated) - amount of profit the company has made
* currentRate         - The current calculated unit of work
* currentUpgradePrice - The current calculated upgrade price
* currentInterval     - The current interval (in ms) per unit of work
* numUpgrades (not currently used) - Number of upgrades performed so far
**************************************************************************/
const createCompany = (name='', active=false, businessCost=0, managerCost=0, baseRate=1, baseUpgradePrice=1, baseInterval=1000) => ({
  active,
  businessCost,
  name,
  changed: false,
  managerHireTime: null,
  managerCost,
  lastUpdated: null, 
  baseRate,
  baseUpgradePrice,
  baseInterval,
  currentProfit: 0,
  currentRate: baseRate,
  currentUpgradePrice: baseUpgradePrice,
  currentInterval: baseInterval,
  numUpgrades: 0
});

/**************************************************************************
GameEngine - The heart of the game, runs the game, and delivers data to 
relevant consumers, such as the ui
* Think of the "M" in MVC
**************************************************************************/
const GameEngine = (companiesList) => {
  let _totalProfit = 0;
  if(Array.isArray(companiesList) === false) { console.error('Error initializing game engine, companies list was corrupt.'); }
  
  // NOTE: List of companies is 0 indexed
  let _list = companiesList;

  // Make a shallow immutable copy by value of the original list so we can reset
  const defaultState = _list.map( obj => Object.freeze( Object.assign({}, obj) ) );

  const has = (index) => typeof _list[index] !== 'undefined';
  
  // getAllCompanies & getCompany provide immutable copies of the data to the program
  // Object.freeze to prevent consumers from making direct changes to the game
  const getAllCompanies = () => _list.map( obj => Object.freeze(Object.assign({}, obj)) );
  const getCompany = (index) => Object.freeze( Object.assign({}, _list[index]) );


  const getTotalProfit = () => _totalProfit;
  const updateTotalProfit = (newTotalProfit) => { 
    if(isNaN(newTotalProfit) === true) { return false; } 
    _totalProfit = newTotalProfit;
    return true;
  };

  const getProgress = (index) => {
    if(_list[index] == null) { return -99; }

    return Math.trunc( (Date.now() - _list[index].lastUpdated) / _list[index].currentInterval * 100);
  };

  const getLastUpdated = (index) => (_list[index] == null) ? 0 : _list[index].lastUpdated;
  const setLastUpdated = (index, timestamp) => {
    if(_list[index] == null) { return false; } 
    if(isNaN(timestamp) === true) { return false; }

    _list[index] = Object.assign(_list[index], { lastUpdated: timestamp });
    return true;
  };

  const setChanged = (index, changed) => {
    if(_list[index] == null) { return false; } 
    _list[index] = Object.assign(_list[index], { changed:  (_list[index].changed === true) ? true : false });
    return true;
  };
  const hasChanged = index => (_list[index] == null) ? false : _list[index].changed;

  const work = (index) => {
    if(_list[index] == null) { return false; }

    _list[index] = Object.assign(_list[index], { 
      currentProfit: _list[index].currentProfit + _list[index].currentRate,
      changed: true
    });

    _totalProfit += _list[index].currentRate;
    return true;
  };

  const canBuy = (index) => (_list[index] == null) ? false : _totalProfit >= _list[index].businessCost; 
  const buy = (index) => {
    if(canBuy(index) === false) { return false; }

    _totalProfit -= _list[index].businessCost;
    _list[index] = Object.assign(_list[index], { active: true });
    return true;
  }; 

  const canUpgrade = (index) => (_list[index] == null) ? false : _totalProfit >= _list[index].currentUpgradePrice; 
  const upgrade = (index) => {
    if(canUpgrade(index) === false) { return false; }

    _totalProfit -= _list[index].currentUpgradePrice;

    // TODO: Change upgrade, rate, and interval modifiers to be configurable
    _list[index] = Object.assign(_list[index], { 
      numUpgrades: _list[index].numUpgrades + 1,
      currentUpgradePrice: _list[index].currentUpgradePrice * 1.5,
      currentRate: _list[index].currentRate * 1.1,
      currentInterval: _list[index].currentInterval * 0.9 - (0.05/(_list[index].numUpgrades + 1)),
    });

    return true;
  }; 

  const canHireManager = (index) => (_list[index] == null) ? false : _totalProfit >= _list[index].managerCost; 
  const hireManager = (index) => {
    if(canHireManager(index) === false) { return false; }

    const now = Date.now();
    _list[index] = Object.assign(_list[index], { 
      managerHireTime: now,
      lastUpdated: now,
    });

    _totalProfit -= _list[index].managerCost;
    return false;
  };


  /**************************************************************************
  checkManagers - Perform an update of profits 
  **************************************************************************/
  const checkManagers = () => {
    _list = _list.map( company => {
      let noChangesCompany = Object.assign(company, { });
      if(company.managerHireTime == null) { return noChangesCompany; }

      // If a company has exceeded its last update by at least one interval
      // We:
      // * Get the number of intervals since the last update
      // * Updated currentProfit and lastUpdated based on # of intervals since last update
      if(Date.now() - company.lastUpdated > company.currentInterval) {
        const numIntervals = Math.trunc( (Date.now() - company.lastUpdated) / company.currentInterval);

        _totalProfit += company.currentRate * numIntervals;

        return Object.assign(company, { 
          currentProfit: company.currentProfit + (company.currentRate * numIntervals),
          changed: true,
          lastUpdated: company.lastUpdated + (company.currentInterval * numIntervals) 
        });
      }
      return noChangesCompany;
    });

    return true;
  };

  /**************************************************************************
  saveToStorage - saves the current state of the game to localStorage on the browser
  **************************************************************************/
  const saveToStorage = () => {
    const objToSave = { totalProfit: _totalProfit, companies: getAllCompanies() };
    localStorage.setItem('gameEngineState', JSON.stringify(objToSave) );
    return true;
  };

  /**************************************************************************
  loadFromStorage - load the state of the game from local storage
  **************************************************************************/
  const loadFromStorage = () => {
    const gameEngineState = JSON.parse( localStorage.getItem('gameEngineState') );
    const errorMessage = 'Unable to retrieve game state';
  
    // if gameEngineState is null, there is nothing to load
    if(localStorage.getItem('gameEngineState') == null) { return true; }
    
    if(gameEngineState == null || gameEngineState.length == 0) { console.log(`${errorMessage}, gameEngineState may be corrupt.`); return false; }
    if( Array.isArray(gameEngineState.companies) === false) { console.log(`${errorMessage}, unable to retrieve companies.`); return false; }
    
    _totalProfit = gameEngineState.totalProfit;
    _list = gameEngineState.companies;
    return true;
  };

  /**************************************************************************
  reset - return the game to the default state
  **************************************************************************/
  const reset = () => { 
    localStorage.removeItem('gameEngineState');

    _list = Array.from(defaultState).map( obj => Object.assign({}, obj) );
    _totalProfit = 0;
    return true;
  };

  /**************************************************************************
  To be used only for testing ;)
  **************************************************************************/
  const cheat = (amount) => {
    if(isNaN(amount)) { console.log('No cheating!!!'); return; }
    _totalProfit = amount;
    return true;
  };

  /**************************************************************************
  Non public methods
  * These were used during development, and left here in case future versions 
    require this API
  * Public references to these methods are currently removed
  **************************************************************************/
  const getBusinessCost = (index) => (_list[index] == null) ? -999999 : _list[index].businessCost;
  const getActive = (index) => (_list[index] == null) ? false : _list[index].active;
  const setActive = (index, active) => { 
    if(_list[index] == null) { return false; } 
    _list[index] = Object.assign(_list[index], { active: (active === true) ? true : false });
    return true;
  };

  const getProfit = (index) => (_list[index] == null) ? 0 : _list[index].currentProfit;
  const setProfit = (index, profit) => {
    if(_list[index] == null) { return false; } 
    _list[index] = Object.assign(_list[index], { currentProfit: profit });
    return true;
  };

  const getManagerHireTime = (index) => (_list[index] == null) ? 0 : _list[index].managerHireTime;
  const setManagerHireTime = (index, timestamp) => {
    if(_list[index] == null) { return false; } 
    if(isNaN(timestamp) === true) { return false; }
    _list[index] = Object.assign(_list[index], { managerHireTime: timestamp });
    return true;
  };

  return { 
    createCompany,

    has, 
    getAllCompanies, getCompany, 
    getTotalProfit, updateTotalProfit,
    getProgress,

    setChanged, hasChanged,
    getLastUpdated, setLastUpdated,

    work,
    canBuy, buy,
    canUpgrade, upgrade,
    canHireManager, hireManager,

    checkManagers,

    saveToStorage, loadFromStorage,
    reset,
    cheat
  };
};

export { createCompany, GameEngine };