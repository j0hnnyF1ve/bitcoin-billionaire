/**************************************************************************
Handlers - The HTML DOM uses these to interact with the Game Engine
* Think of the "C" in MVC
**************************************************************************/
const Handlers = (gameEngine, UI, Helpers) => {

  const buyBusiness = e => {
    let index = e.target.closest('.company').dataset.index;
    if(index == null) { return; }
    if(gameEngine.canBuy(index) === false) { console.log(`Can't buy Business ${index}: not enough funds`); return; }
  
    gameEngine.buy(index);
  };
  
  const upgradeBusiness = e => {
    let index = e.target.closest('.company').dataset.index;
    if(index == null) { return; }
    if(gameEngine.canUpgrade(index) === false) { console.log(`Can't upgrade Business ${index}: not enough funds`); return; }
  
    gameEngine.upgrade(index);
  };
  
  const hireManager = e => {
    let index = e.target.closest('.company').dataset.index;
    if(index == null) { return; }
    if(gameEngine.canHireManager(index) === false) { console.log(`Can't hire Manager ${index}: not enough funds`); return; }
  
    gameEngine.hireManager(index);
  };
  
  const work = e => {
    let index = e.target.closest('.company').dataset.index;
    if(index == null) { return; }
  
    const target = (e.target.nodeName != 'BUTTON') ? e.target.closest('button') : e.target;
    
    target.className = 'work active';
    target.disabled = true;
    gameEngine.setLastUpdated(index, Date.now() );

    setTimeout( () => { 
      // Fixes a bug where having work occurring while hiring a manager causes
      // the work button to show up afterwards
      if(gameEngine.getCompany(index).managerHireTime == null) {
        target.className = 'work';
        target.disabled = false;
      }
      gameEngine.work(index); 
    }, gameEngine.getCompany(index).baseInterval);
  };

  const reset = e => gameEngine.reset();

  return {
    buyBusiness,
    upgradeBusiness,
    hireManager,
    work,
    reset
  };
};

export { Handlers };