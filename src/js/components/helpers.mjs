const Helpers = () => {
  const hideEl = (el) => {
    if(el == null) { return; }
    el.className = el.className.replace('hide', '').replace(' hide', '').trim() + ' hide';
  };
  
  const showEl = (el) => {
    if(el == null) { return; }
    el.className = el.className.replace('hide', '').replace(' hide', '').trim();
  };

  return { hideEl, showEl };
};

export { Helpers };